// create for input data from database
'use strict'
var mongoose = require('mongoose')
var date = require('date-and-time')

var now = date.format(new Date(),'YYYY/MM/DD HH:mm:ss')
var Schema = mongoose.Schema

var PlaylistSchema = new Schema({
  name: {type : String , default: ''},
  desc: {type : String , default: ''},
  lastestUpdate: {type : String , default: now},
  num:  {type : String , default: 0},
  list: [{type : String , default: ''}],
  img : {type : String,default:'https://cdn.discordapp.com/attachments/701032411290402826/843804124864184360/pU3bnutJU91u3b4IeRPQTOP8jhV.jpg'}
})

var GenreListSchema = new Schema({
  g_id : {type : String , default: ''},
  name : {type : String , default: ''},
  count : {type: Number , default: 1}
})

var LikeListSchema = new Schema({
  movies_id: [{type : String , default: ''}],
  genres: [{ type: GenreListSchema, default: () => ({})}],
})

var CommentSchema = new Schema({
  user_id: {type : String , default: ''},
  user_name: {type : String , default: ''},
  comment: {type : String , default: ''},
  time: {type : String , default: now},
})

var AccountSchema = new Schema({
  userName :  {type : String , default: ''},
  email :  {type : String , default: ''} ,
  password: {type : String ,default: ''} ,
  firstName : {type : String , default: ''},
  lastName : {type : String , default: ''},
  gender :  {type : String , default: '' } ,
  dob : {type : Date ,default: '' } ,
  phone :  {type : String , default: ''} ,
  playlists : [{ type: PlaylistSchema, default: () => ({})}],
  likelists : { type: LikeListSchema, default: () => ({})},
  img:{type : String , default: ''}
})

var movieSchema = new Schema({
  movie_id:{type : String , default: ''},
  comments:[{ type: CommentSchema, default: () => ({})}],
  rate_avg:{ type: Number ,default: 0},
  rate_count:{ type: Number ,default: 0}
})

module.exports.Account = mongoose.model('Accounts', AccountSchema ,'Accounts')
module.exports.Movie = mongoose.model('Movies', movieSchema ,'Movies') //(<collection_name>,<schema>)