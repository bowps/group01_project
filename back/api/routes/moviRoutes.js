module.exports = function(app){
    var controller = require('../controllers/moviController')

    //====== Accounts ======//
    app.route('/account/getall')
    .get(controller.listAllAccount)
    app.route('/account/create')
    .post(controller.createAAccount)

    app.route('/account/get/:id')
    .get(controller.readAAccountbyId)
    app.route('/account/getbyemail/:email')
    .get(controller.readAAccountbyEmail)
    app.route('/account/update/:id')
    .put(controller.updateAAccountbyId)
    app.route('/account/delete/:id')
    .delete(controller.deleteAAccountbyId)

    app.route('/account/getallcomment')
    .get(controller.listAllAccountComment)

    //====== PlayLists ======//
    app.route('/playlist/getall')
    .get(controller.ListAllPlaylist)
    app.route('/playlist/create')
    .post(controller.createAPlaylist)

    app.route('/playlist')
    .get(controller.readAPlayListbyId)
    .put(controller.updateAPlayListbyId)
    .delete(controller.deleteAPlayListbyId)

    app.route('/playlist/addMovie')
    .put(controller.addAMovieToPlayList)
    app.route('/playlist/delMovie')
    .delete(controller.deleteAMovieInPlayList)

    //====== LikeLists ======//
    app.route('/LikeList/get')
    .get(controller.getLikelistData)

    app.route('/LikeList/addMovie')
    .put(controller.addAMovieToLikeList)
    app.route('/LikeList/delMovie')
    .delete(controller.deleteAMovieInLikeList)

    //====== Movie Comments ======//
    app.route('/movie/getall')
    .get(controller.listAllmovie)
    app.route('/movie/getmovie')
    .get(controller.readAMoviebymovieId)

    app.route('/movie/calrate')
    .put(controller.calculateRateMovieId)

    app.route('/comment/create')
    .post(controller.createAComment)
    app.route('/comment')
    .get(controller.getACommentbyId)
    .put(controller.updateACommentbyId)
    .delete(controller.deleteACommentbyId)
}