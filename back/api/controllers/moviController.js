'use strict'

var mongoose = require('mongoose')
var date = require('date-and-time')
Account = mongoose.model('Accounts')
Movie = mongoose.model('Movies')
var axios = require('axios')

//=========== Accounts ===========//

exports.listAllAccount = function (req, res) {
    Account.find({}, null, null, function (err, account) {
        if (err) throw err
        res.json(account)
    })
}

exports.createAAccount = function (req, res) {
    var newAccount = new Account(req.body)
    newAccount.save(function (err, account) {
        if (err) throw err
        const response = {
            message: "Create a account successfully",
            data: account
        }
        res.json(response)
    })
}

exports.readAAccountbyId = function (req, res) {
    Account.findById(req.params.id, function (err, account) {
        if (err) console.error(err)
        res.json(account)
    })
}

exports.readAAccountbyEmail = function (req, res) {
    Account.findOne({
        email: req.params.email
    }, function (err, account) {
        if (err) console.error(err)
        res.json(account)
    })
}

exports.updateAAccountbyId = function (req, res) {
    var newaccount = {}
    newaccount = req.body
    Account.findByIdAndUpdate(req.params.id, newaccount, {
        new: true
    }, function (err, account) {
        if (err) throw err
        const response = {
            message: "Update a account successfully",
        }
        res.json(response)
    })
}

exports.deleteAAccountbyId = function (req, res) {
    Account.findByIdAndRemove(req.params.id, function (err, account) {
        if (err) throw err
        const response = {
            message: "Delete a account successfully",
        }
        res.json(response)
    })
}

exports.listAllAccountComment = function (req, res) {
    var acc_id = req.query.aid
    Movie.find({}, null, null, function (err, movie) {
        if (err) throw err
        var commentList = []
        movie.map(movie => {
            movie.comments.map(result => {
                if (result.user_id == acc_id) {
                    let obj = {
                        "movie_id": movie.movie_id,
                        "comments": {
                            "detail": result.comment,
                            "time": result.time,
                            "_id": result._id
                        }
                    }
                    commentList.push(obj)
                }
            })

        })
        res.json(commentList)
    })
}
//=========== LikeList ===========//

exports.getLikelistData = function (req, res) {
    var acc_id = req.query.aid
    Account.findById(acc_id, null, function (err, account) {
        if (err) throw err
        let likelist = account.likelists
        res.json(likelist)
    })
}

exports.addAMovieToLikeList = function (req, res) {
    var acc_id = req.query.aid
    var api_key = 'b6c502901261dc2e049df4d247bd97be'
    Account.findById(acc_id, null, async function (err, account) {
        if (err) throw err

        var likelist = account.likelists
        var mv_id = req.body.movie_id
        var count = 0
        var last = mv_id.length
        await mv_id.forEach(async mv_id => {
            if (likelist.movies_id.indexOf(mv_id) == -1) {
                likelist.movies_id.push(mv_id)
                await axios.get('https://api.themoviedb.org/3/movie/' + mv_id + '?api_key=' + api_key + '&language=en-US')
                    .then((response) => {
                        let genreMV = response.data.genres

                        if (likelist.genres.length == 0) {
                            genreMV.forEach(result => {
                                likelist.genres.push({
                                    "g_id": result.id,
                                    "name": result.name,
                                    "count": 1
                                })
                            })
                        } else {
                            var temp = []
                            likelist.genres.filter(genre => {
                                genreMV.filter(result => {
                                    if (genre.g_id == result.id) {
                                        genre.count = parseInt(genre.count) + 1
                                        temp.push(result)
                                    }

                                })
                            })
                            genreMV.filter(arr => {
                                if (!temp.includes(arr)) {
                                    likelist.genres.push({
                                        "g_id": arr.id,
                                        "name": arr.name,
                                        "count": 1
                                    })
                                }
                            })

                        }
                        likelist.genres = likelist.genres.sort(CompareData("count"))
                        count = count + 1
                        /* if (count == last) {
                            console.log(likelist)
                            account.save(function (err, data) {
                                if (err) throw err
                                const response = {
                                    message: "Add movie  to LikeList successfully",
                                    data: data.likelists
                                }
                                res.json(response)
                            })
                        } */
                    })
                    .catch((error) => {
                        console.log(error) // should have table name of msg Error
                    })

            } else {
                console.log("Alrady have movie : "+mv_id+" in LikedList")
                count = count + 1
            }
            if (count == last) {
                account.save(function (err, data) {
                    if (err) throw err
                    const response = {
                        message: "Add movie  to LikeList successfully",
                        data: data.likelists
                    }
                    res.json(response)
                })
            }
        })
    })
}

exports.deleteAMovieInLikeList = function (req, res) {
    var acc_id = req.query.aid
    var api_key = 'b6c502901261dc2e049df4d247bd97be'
    Account.findById(acc_id, null, async function (err, account) {
        if (err) throw err

        let likelist = account.likelists
        var mv_id = req.body.movie_id
        if (likelist.movies_id.indexOf(mv_id) >= 0) {
            let index = likelist.movies_id.indexOf(mv_id)
            likelist.movies_id.splice(index, 1)

            await axios.get('https://api.themoviedb.org/3/movie/' + mv_id + '?api_key=' + api_key + '&language=en-US')
                .then((response) => {
                    let genreMV = response.data.genres

                    likelist.genres.filter(genre => {
                        genreMV.filter(result => {
                            if (genre.g_id == result.id) {
                                if (genre.count == 0) {
                                    genre.count = 0
                                } else {
                                    genre.count = parseInt(genre.count) - 1
                                }
                            }

                        })
                    })
                    likelist.genres = likelist.genres.sort(CompareData("count"))
                })
                .catch((error) => {
                    console.log(error) // should have table name of msg Error
                })

            account.save(function (err, data) {
                if (err) throw err
                const response = {
                    message: "Delete movie : " + mv_id + " to LikeList successfully",
                    data: data.likelists
                }
                res.json(response)
            })
        } else {
            let status = {
                message: "This movie is not in Likelist"
            }
            res.json(status)
        }
    })
}

//=========== PlayLists ===========//

exports.ListAllPlaylist = function (req, res) {
    var acc_id = req.query.aid
    Account.findById(acc_id, null, function (err, account) {
        if (err) throw err
        var playlists
        if (account.playlists == null){
            playlists = []
        }
        else {
            playlists=account.playlists
        }
        
        res.json(playlists)
    })
}

exports.createAPlaylist = function (req, res) {
    var acc_id = req.query.aid
    Account.findById(acc_id, null, function (err, account) {
        if (err) throw err

        account.playlists.push(req.body)
        account.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Create a playlist successfully",
                data: {
                    email: data.email,
                    playlist: data.playlists
                }
            }
            res.json(response)
        })
    })
}

exports.readAPlayListbyId = function (req, res) {
    var acc_id = req.query.aid
    var pl_id = req.query.pid
    Account.findById(acc_id, function (err, account) {
        if (err) console.error(err)
        let playlist = account.playlists.id(pl_id)
        res.json(playlist)
    })
}

exports.updateAPlayListbyId = function (req, res) {
    var acc_id = req.query.aid
    var pl_id = req.query.pid
    Account.findById(acc_id, function (err, account) {
        if (err) throw err

        let playlist = account.playlists.id(pl_id)
        if (req.body.name == "" || req.body.name == null){
            playlist.name = playlist.name
        }else {
            playlist.name = req.body.name
        }

        if (req.body.desc == "" || req.body.desc == null){
            playlist.desc = playlist.desc
        }else {
            playlist.desc = req.body.desc
        }
        /* if (playlist.img == null || playlist.img == "" || playlist.img == "https://cdn.discordapp.com/attachments/701032411290402826/843804124864184360/pU3bnutJU91u3b4IeRPQTOP8jhV.jpg"){
            playlist.img = req.body.img
        }else{
            playlist.img = playlist.img
        } */
        playlist.img = req.body.img
        playlist.lastestUpdate = date.format(new Date(), 'YYYY/MM/DD HH:mm:ss')
        
        playlist.list = playlist.list
       
        console.log(playlist.lastestUpdate)
        account.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Update a " + playlist.name + " playlist successfully",
            }
            res.json(response)
        })
    })
}

exports.deleteAPlayListbyId = function (req, res) {
    var acc_id = req.query.aid
    var pl_id = req.query.pid
    Account.findById(acc_id, function (err, account) {
        if (err) throw err

        let playlist = account.playlists.id(pl_id)
        account.playlists.id(pl_id).remove()
        playlist.num = playlist.list.length
        playlist.lastestUpdate = date.format(new Date(), 'YYYY/MM/DD HH:mm:ss')
        account.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Delete a " + playlist.name + " playlist successfully",
            }
            res.json(response)
        })
    })
}

exports.addAMovieToPlayList = function (req, res) {
    var acc_id = req.query.aid
    var pl_id = req.query.pid
    Account.findById(acc_id, null, function (err, account) {
        if (err) throw err

        let playlist = account.playlists.id(pl_id)
        playlist.name = playlist.name
        playlist.desc = playlist.desc
        var mv_id = req.body.movie_id
        var check = -1
        if (playlist.list.length == 0) {
            playlist.list.push(mv_id)
            
        } else if (playlist.list.indexOf(mv_id) == -1) {
            playlist.list.push(mv_id)
            
        } else {
            console.log("Movie already in your playlist")
            check = 1
        }
        playlist.num = playlist.list.length
        playlist.lastestUpdate = date.format(new Date(), 'YYYY/MM/DD HH:mm:ss')
        
        account.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Add movie : " + mv_id + " successfully",
                check: check
            }
            res.json(response)
        })
    })
}

exports.deleteAMovieInPlayList =  function (req, res) {
    var acc_id = req.query.aid
    var pl_id = req.query.pid
    Account.findById(acc_id,async function (err, account) {
        if (err) throw err

        let playlist = account.playlists.id(pl_id)
        var mv_id = req.body.movie_id
        if (playlist.list.indexOf(mv_id) >= 0) {
            let index = playlist.list.indexOf(mv_id)
            playlist.list.splice(index, 1)
            playlist.num = playlist.list.length
            if (playlist.num != 0 ){
                var mv_id =  playlist.list[0]
                await axios.get("https://api.themoviedb.org/3/movie/" + mv_id + "?api_key=b6c502901261dc2e049df4d247bd97be&language=en-US")
                .then((response) => {
                    playlist.img = 'http://image.tmdb.org/t/p/w500/' + response.data.poster_path
                });
            }
            else{
                playlist.img = 'https://cdn.discordapp.com/attachments/701032411290402826/843804124864184360/pU3bnutJU91u3b4IeRPQTOP8jhV.jpg'
            }
        account.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Delete movie : " + mv_id + " successfully",
            }
            res.send(response)
        })
    }
    })
}

//=========== Movie ===========//

exports.listAllmovie = function (req, res) {
    Movie.find({}, null, null, function (err, movie) {
        if (err) throw err
        res.json(movie)
    })
}

exports.readAMoviebymovieId = function (req, res) {
    var mv_id = req.query.mid
    Movie.findOne({
        movie_id: mv_id
    }, function (err, movie) {
        if (err) throw err

        if (movie == null) {
            var newMovie = new Movie({
                movie_id: mv_id,
            })
            newMovie.save(function (err, data) {
                if (err) throw err
                const response = {
                    message: "Create a movie successfully",
                    data: data
                }
                res.json(data)
            })
        } else {
            res.json(movie)
        }
    })
}

//=========== Movie Rate ===========//

exports.calculateRateMovieId = async function (req, res) {
    var mv_id = req.query.mid
    var rate_value = req.body.value
    Movie.findOne({
        movie_id: mv_id
    }, function (err, movie) {
        if (err) throw err

        let oldcount = movie.rate_count
        movie.rate_count = movie.rate_count + 1

        movie.rate_avg = ((parseFloat(movie.rate_avg)*(parseInt(oldcount)) + parseInt(rate_value))) / movie.rate_count
        movie.rate_avg = movie.rate_avg.toFixed(2)

        movie.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Update rate for movie : " + mv_id + " successfully",
                data: data
            }
            res.json(response)
        })
    })
}

//=========== Movie Comments ===========//

exports.createAComment = function (req, res) {
    var mv_id = req.query.mid
    Movie.findOne({
        movie_id: mv_id
    }, null, function (err, movie) {
        if (err) throw err
        movie.comments.push(req.body)
        movie.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Create a comment successfully",
            }
            res.json(response)
        })
    })
}

exports.getACommentbyId = function (req, res) {
    var cm_id = req.query.cid
    var mv_id = req.query.mid
    Movie.findOne({
        movie_id: mv_id
    }, function (err, movie) {
        if (err) console.error(err)
        let comment = movie.comments.id(cm_id)
        res.json(comment)
    })
}


exports.updateACommentbyId = function (req, res) {
    var acc_id = req.query.aid
    var cm_id = req.query.cid
    var mv_id = req.query.mid
    Movie.findOne({
        movie_id: mv_id
    }, function (err, movie) {
        if (err) throw err

        let comment = movie.comments.id(cm_id)
        if (comment.user_id == acc_id) {
            comment.user_id = req.body.user_id
            comment.user_name = req.body.user_name
            comment.comment = req.body.comment
        } else {
            let status = {
                message: "You cannot edit other's comment"
            }
            console.log(status)
        }

        movie.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Update a comment successfully",
                data: data
            }
            res.json(response)
        })
    })
}

exports.deleteACommentbyId = function (req, res) {
    var acc_id = req.query.aid
    var cm_id = req.query.cid
    var mv_id = req.query.mid
    Movie.findOne({
        movie_id: mv_id
    }, function (err, movie) {
        if (err) throw err

        let comment = movie.comments.id(cm_id)
        if (comment.user_id == acc_id) {
            movie.comments.id(cm_id).remove()
        } else {
            let status = {
                message: "You cannot delete other's comment"
            }
            console.log(status)
        }

        movie.save(function (err, data) {
            if (err) throw err
            const response = {
                message: "Delete a comment : " + comment._id + " successfully",
            }
            res.json(response)
        })
    })
}

//==================================//
function CompareData(prop) {
    return function (a, b) {
        if (a[prop] < b[prop]) {
            return 1;
        } else if (a[prop] > b[prop]) {
            return -1;
        }
        return 0;
    }
}