import { createRouter, createWebHistory } from 'vue-router'
import firebase from 'firebase'
import SignIn from '../views/SignIn.vue'
import SignUp from '../views/SignUp.vue'
import MovieList from '../views/MovieList.vue'
import Account from '../views/Account.vue'
import Recommend from '../views/Recommend.vue'
import LikeList from '../views/LikeList.vue'
import Help from '../views/Help.vue'
import Playlist from '../views/Playlist.vue'
import MovieDetail from '../views/MovieDetail.vue'
import PlaylistDetail from '../views/PlaylistDetail.vue'

const routerHistory = createWebHistory()
const routes = [
    {
        path: '/',
        redirect: '/signin'
      },
      {
        path: '/:catchAll(.*)',
        redirect: '/signin'
      },
      {
        path: '/movies',
        name: 'MovieList',
        component: MovieList,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/signin',
        name: 'SignIn',
        component: SignIn
      },
      {
        path: '/signup',
        name: 'SignUp',
        component: SignUp
      },
      {
        path: '/account',
        name: 'Account',
        component: Account,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/recommend',
        name: 'Recommend',
        component: Recommend,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/likelist',
        name: 'LikeList',
        component: LikeList,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/help',
        name: 'Help',
        component: Help,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/playlist',
        name: Playlist,
        component: Playlist,
        meta: {
          requiresAuth: true
        }
      }, 
      {
        path: '/moviedetail/:movieId',
        name: 'MovieDetail',
        component: MovieDetail,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/playlistdetail/:playlistId',
        name: PlaylistDetail,
        component: PlaylistDetail,
        meta: {
          requiresAuth: true
        }
      }
]

const router = createRouter({
	history: routerHistory,
	routes
})

router.beforeEach((to, from, next) => {
    const currentUser = firebase.auth().currentUser
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
    if (requiresAuth && !currentUser) {
      console.log("You are not authorized to access this area.");
      next('signin')
    } else if (!requiresAuth && currentUser) {
      console.log("You are authorized to access this area.");
      next('movies')
    } else {
      next()
    }
  })

export default router