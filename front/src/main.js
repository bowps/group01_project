import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './index.css'
import firebase from 'firebase'


let app
var firebaseConfig = {
    apiKey: "AIzaSyCDmF0YOUCrVAIBvNPPQWO5LTjXJqayKY0",
    authDomain: "movi-firebase.firebaseapp.com",
    projectId: "movi-firebase",
    storageBucket: "movi-firebase.appspot.com",
    messagingSenderId: "1047465051440",
    appId: "1:1047465051440:web:478dac7863fad2a6a4f4f5",
    measurementId: "G-KZX0PBV1QD"
  };
  
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
firebase.auth().onAuthStateChanged((user)=>{
    if(!app){
        createApp(App).use(router).mount('#app') // => mounted to App.vue by router
    }
})
